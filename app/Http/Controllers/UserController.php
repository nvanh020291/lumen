<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;



class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getListUser(){

        $users   =    DB::table('users')->get();
        return $users;


    }


}
